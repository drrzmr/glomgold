lazy val root = (project in file("."))
  .settings(
    name := "glomgold",
    organization := "dev.uhet",
    licenses += "Apache License, Version 2.0" -> url("https://www.apache.org/licenses/LICENSE-2.0"),
    version := "1.0.0",
    scalaVersion := "2.12.8",
    libraryDependencies ++= Seq(
      "com.amazonaws" % "aws-lambda-java-core" % "1.2.0",
      "com.amazonaws" % "aws-lambda-java-events" % "2.2.6",

      "software.amazon.awssdk" % "ec2" % "2.5.56",
      "software.amazon.awssdk" % "elasticloadbalancingv2" % "2.5.56",
      "software.amazon.awssdk" % "autoscaling" % "2.5.56",
      "software.amazon.awssdk" % "bom" % "2.5.56" pomOnly(),

      "org.scalatest" %% "scalatest" % "3.0.5" % "test"
    )
  )

javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint")


assemblyMergeStrategy in assembly := {
  case PathList("META-INF", _*) => MergeStrategy.discard
  case _ => MergeStrategy.first
}
