package dev.uhet.glomgold

import dev.uhet.glomgold.extension.model.ModelExtension
import dev.uhet.glomgold.model.AutoScalingGroupName
import dev.uhet.glomgold.model.LoadBalancerName
import dev.uhet.glomgold.model.TargetGroupName
import software.amazon.awssdk.services.ec2.model.Instance

class Logger(instanceSet: Set[Instance],
             lbName: LoadBalancerName,
             asgName: AutoScalingGroupName,
             tgName: TargetGroupName) extends ModelExtension {

  val prefix: String = s"${lbName.name} ${asgName.name} ${tgName.name} ${instanceSet.onDemand.size} " +
    s"${instanceSet.spot.size}"

  def log(str: String): Unit = println(s"$prefix, $str")
}

object Logger {
  def apply(instanceSet: Set[Instance],
            lbName: LoadBalancerName,
            asgName: AutoScalingGroupName,
            tgName: TargetGroupName): String => Unit =
    new Logger(instanceSet, lbName, asgName, tgName).log _
}
