package dev.uhet.glomgold

import dev.uhet.glomgold.client.ASGClient
import dev.uhet.glomgold.client.EC2CloneClient
import dev.uhet.glomgold.exception.EC2CloneException
import dev.uhet.glomgold.extension.model.ModelExtension
import dev.uhet.glomgold.model.AutoScalingGroupName
import dev.uhet.glomgold.model.InstanceId
import dev.uhet.glomgold.model.LoadBalancerName
import dev.uhet.glomgold.model.TargetGroupName
import software.amazon.awssdk.services.autoscaling.model.AutoScalingGroup
import software.amazon.awssdk.services.elasticloadbalancingv2.model.TargetHealthStateEnum

import scala.util.Failure
import scala.util.Success

class Runner extends ModelExtension {

  lazy val db: Database = Database.load(Environment.LOAD_BALANCER_NAMES) match {
    case Failure(e) =>
      println(s">> fail to retrieve aws data")
      throw e

    case Success(database) => database
  }

  def run(lbName: LoadBalancerName, asgName: AutoScalingGroupName, tgName: TargetGroupName): Unit = {
    import software.amazon.awssdk.services.ec2.model.Instance
    import software.amazon.awssdk.services.elasticloadbalancingv2.model.TargetHealthDescription

    val thdSet: Set[TargetHealthDescription] = db.thdMap(tgName)
    val instanceSet: Set[Instance] = db.tgInstanceMap(tgName)

    val log = Logger(instanceSet, lbName, asgName, tgName)

    thdSet.health.filter(_._2 != TargetHealthStateEnum.HEALTHY)
      .foreach { case (instanceId, health) =>
        log(s"unhealthy instance: ${instanceId.id}, status: $health")
      }

    instanceSet.spot.headOption
      .foreach(_ =>
        log(s"spot(s): ${instanceSet.spot.id.map(instanceId => instanceId.id).mkString(", ")}"))

    instanceSet.onDemand.headOption
      .foreach(_ =>
        log(s"onDemand(s): ${instanceSet.onDemand.id.map(instanceId => instanceId.id).mkString(", ")}"))

    val clonedIdSet = instanceSet.spot.tagValue("GlomgoldOriginalInstanceId").map(InstanceId(_)).toSet
    val aliveClonedIdSet = clonedIdSet.intersect(instanceSet.id.toSet)
    val toDie = aliveClonedIdSet.headOption

    clonedIdSet.headOption
      .foreach(_ =>
        log(s"onDemand cloned: ${clonedIdSet.map(_.id).mkString(", ")}"))

    aliveClonedIdSet.headOption
      .foreach(_ =>
        log(s"onDemand cloned and alive: ${aliveClonedIdSet.map(_.id).mkString(", ")}"))

    toDie.foreach(instanceId =>
      log(s"onDemand to die: ${instanceId.id}"))

    if (instanceSet.onDemand.size <= Environment.ON_DEMAND_MIN_INSTANCES)
      log(s"onDemand min instances: ${Environment.ON_DEMAND_MIN_INSTANCES}, actual: ${instanceSet.onDemand.size}")
    else if (thdSet.notHealthy)
      log("unhealthy, try again soon")
    else {

      /* clean */
      toDie.foreach { instanceId =>
        ASGClient.terminateInstance(instanceId) match {
          case Failure(exception) => throw exception
          case Success(_) => log(s"killed: ${instanceId.id}")
        }
      }

      /* clone */
      instanceSet.onDemand.headOption.foreach { onDemand =>
        log(s"onDemand to clone: ${onDemand.id.id}")

        val tryClone = EC2CloneClient(onDemand, Environment.SPOT_MAX_PRICE).run()
        tryClone.foreach { case (spot, sir) =>
          log(s"onDemand cloned: ${onDemand.id.id}, to: ${spot.id.id}, sir: ${sir.id.id}")
        }

        val tryAttach = tryClone
          .flatMap { case (spot, _) => ASGClient.attachInstance(asgName, spot.id) }
          .recover {
            case e: EC2CloneException => log(s"clone fail for instance: ${e.instanceId.id}, sir: ${e.sirId.id}, " +
              s"message: ${e.getMessage}")
              InstanceId.empty
          }

        tryAttach match {
          case Success(instanceId) => log(s"successful attached: ${instanceId.id}, asg: ${asgName.name}")
          case Failure(exception) => throw exception
        }
      }

    }
  }

  def filterAsg(asg: AutoScalingGroup): Boolean =
    asg.desiredCapacity > 0 && asg.minSize > 0 && asg.maxSize > 0

  def run(): Unit = {
    println(">> quack?")
    db.index.foreach {
      case (lbName, asgMap) => asgMap
        .filter { case (asgName, _) => filterAsg(db.asgMap(asgName)) }
        .foreach { case (asgName, tgNameSet) => run(lbName, asgName, tgNameSet.head) }
    }
    println(">> quack, quack!!!")
  }
}

object Runner {
  def apply(): Runner = new Runner()
}
