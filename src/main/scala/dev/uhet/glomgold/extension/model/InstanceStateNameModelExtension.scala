package dev.uhet.glomgold.extension.model

import dev.uhet.glomgold.model.instance._
import software.amazon.awssdk.services.ec2.model.InstanceStateName

trait InstanceStateNameModelExtension {

  implicit class InstanceStateNameModelExtension(state: InstanceStateName) {
    lazy val asScala: State = state match {
      case InstanceStateName.PENDING =>  Pending
      case InstanceStateName.RUNNING => Running
      case InstanceStateName.SHUTTING_DOWN => ShuttingDown
      case InstanceStateName.STOPPED => Stopped
      case InstanceStateName.STOPPING => Stopping
      case InstanceStateName.TERMINATED => Terminated
      case InstanceStateName.UNKNOWN_TO_SDK_VERSION => Unknown
    }
  }

}
