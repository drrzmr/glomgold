package dev.uhet.glomgold.extension.model

import dev.uhet.glomgold.model.spot._
import software.amazon.awssdk.services.ec2.model.SpotInstanceState

trait SpotInstanceStateModelExtension {

  implicit class SpotInstanceStateModelExtension(state: SpotInstanceState) {
    lazy val asScala: State = state match {
      case SpotInstanceState.ACTIVE => Active
      case SpotInstanceState.CANCELLED => Cancelled
      case SpotInstanceState.CLOSED => Closed
      case SpotInstanceState.FAILED => Failed
      case SpotInstanceState.OPEN => Open
      case SpotInstanceState.UNKNOWN_TO_SDK_VERSION => Unknown 
    }
  }

}
