package dev.uhet.glomgold.client

import dev.uhet.glomgold.model.{LoadBalancerName, TargetGroupName}
import software.amazon.awssdk.services.elasticloadbalancingv2.ElasticLoadBalancingV2Client
import software.amazon.awssdk.services.elasticloadbalancingv2.model._

import scala.collection.JavaConverters._
import scala.util.Try

object ELBv2Client extends Client {

  private lazy val client = ElasticLoadBalancingV2Client.builder
    .httpClient(httpSyncClient)
    .build
  
  lazy val findAll: Try[Set[LoadBalancer]] =
    Try(client.describeLoadBalancers.loadBalancers.asScala.toSet)
  
  def lbMap(nameSet: Set[LoadBalancerName]): Try[Map[LoadBalancerName, LoadBalancer]] = {
    val request = DescribeLoadBalancersRequest.builder
      .names(nameSet.map(name => name.name).asJavaCollection)
      .build

    Try(client.describeLoadBalancers(request))
      .map(resp => resp.loadBalancers.asScala
        .map(lb => LoadBalancerName(lb.loadBalancerName) -> lb).toMap)
  }

  def tgMap(lb: LoadBalancer): Try[Map[TargetGroupName, TargetGroup]] = {
    val request = DescribeTargetGroupsRequest.builder
      .loadBalancerArn(lb.loadBalancerArn())
      .build

    Try(client.describeTargetGroups(request))
      .map(resp => resp.targetGroups.asScala
        .map(tg => TargetGroupName(tg.targetGroupName) -> tg).toMap)
  }

  def thdSet(tg: TargetGroup): Try[Set[TargetHealthDescription]] = {
    val request = DescribeTargetHealthRequest.builder
      .targetGroupArn(tg.targetGroupArn)
      .build

    Try(client.describeTargetHealth(request))
      .map(resp => resp.targetHealthDescriptions.asScala.toSet)
  }
}
