package dev.uhet.glomgold.client

import dev.uhet.glomgold.model.AutoScalingGroupName
import dev.uhet.glomgold.model.InstanceId
import software.amazon.awssdk.services.autoscaling.AutoScalingClient
import software.amazon.awssdk.services.autoscaling.model.AttachInstancesRequest
import software.amazon.awssdk.services.autoscaling.model.AutoScalingGroup
import software.amazon.awssdk.services.autoscaling.model.TerminateInstanceInAutoScalingGroupRequest

import scala.collection.JavaConverters._
import scala.util.Try

object ASGClient extends Client {

  private lazy val client = AutoScalingClient.builder.httpClient(httpSyncClient).build

  def findAll: Try[List[AutoScalingGroup]] = {
    Try(client.describeAutoScalingGroups)
      .map(resp => resp.autoScalingGroups.asScala.toList)
  }

  def asgMap: Try[Map[AutoScalingGroupName, AutoScalingGroup]] = {
    Try(client.describeAutoScalingGroups)
      .map(resp => resp.autoScalingGroups.asScala
        .map(asg => AutoScalingGroupName(asg.autoScalingGroupName) -> asg).toMap)
  }

  def terminateInstance(instanceId: InstanceId): Try[Unit] = {
    val request = TerminateInstanceInAutoScalingGroupRequest.builder
      .instanceId(instanceId.id)
      .shouldDecrementDesiredCapacity(true)
      .build

    Try(client.terminateInstanceInAutoScalingGroup(request))
      .map(_ => Unit)
  }

  def attachInstance(asg: AutoScalingGroupName, instanceId: InstanceId): Try[InstanceId] = {
    val request = AttachInstancesRequest.builder
      .autoScalingGroupName(asg.name)
      .instanceIds(instanceId.id)
      .build

    Try(client.attachInstances(request))
      .map(_ => instanceId)
  }
}
