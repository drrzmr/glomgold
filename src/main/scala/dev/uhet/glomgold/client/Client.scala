package dev.uhet.glomgold.client

import software.amazon.awssdk.http.apache.ApacheHttpClient
import software.amazon.awssdk.http.SdkHttpClient

trait Client {

  private[client] lazy val waitTimeoutMs: Option[Int] = Some(1000)

  private[client] lazy val httpSyncClient: SdkHttpClient = ApacheHttpClient.builder
    .maxConnections(10)
    .build()

}
